FROM node:lts
MAINTAINER Alduino (docker@alduino.dev)

RUN curl -L https://raw.githubusercontent.com/pnpm/self-installer/master/install.js | node

WORKDIR /usr/src/app
COPY package.json .
COPY pnpm-lock.yaml .

RUN pnpm install

COPY . .

RUN pnpm run build

ENV PORT=8080

EXPOSE 8080
CMD ["pnpm", "start"]
