**OBSELETE**: Recently, GitLab started offering private package hosting to all users for free. That has made this application obselete, as that was the entire point of it.

RadiantPM has been left in a mostly-complete state, with support for NPM packages currently available. Next.JS seems to have an issue that is causing production builds to not work properly, so if you have any idea what could be causing this please notify us in an issue or create a merge request.

Although RadiantPM in its current form is obselete, we may separate it out from GitLab and AWS to act as a more generic package host, and you are free to do this too in a fork.

---

# RadiantPM

Got to go fast...

(TODO)

## Usage
### Environment variables
All of these environment variables are required.
- `PG_CONNECTION_STRING`: The URL to connect to Postgres. In the format `postgres://username:password@domain:port/database`
- `CDN_PUBLIC_PATH`: The URL to the CDN, including the namespace
- `CDN_NAMESPACE`: The directory to write the packages to
- `CDN_BUCKET_NAME`: The name of the bucket or space
- `ACCESS_KEY_ID`: AWS access key ID
- `ACCESS_KEY_SECRET`: AWS access token
- `S3_ENDPOINT`: Endpoint of an S3-compatible API
- `PORT`: The port that the application will run on

### Files
- `settings.json`: JSON file containing the following keys:
  - `name`: The title of the app
  - `branding`: If true, include RadiantPM branding
- `~/.config/gcloud/application_default_credentials.json`: GCS keyfile

Unfortunately, due to limitations in Next.js, the application settings cannot be changed at runtime, so you will need
to rebuild the web app (`npm run build`) if you change the contents of `settings.json`.

### Docker
If you don't change any settings, you can use our prebuilt docker image (find it under _Packages & Registries_ in the
sidebar). Otherwise, use our Dockerfile as a template and just use a different settings.json file.

Note that, in the default image, the IP is set to 8080. You may map it to something else through Docker.

## License
RadiantPM is licensed under the MIT license. See the LICENSE file for more information.
