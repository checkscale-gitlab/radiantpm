import {NextApiRequest, NextApiResponse} from "next";
import {parseCookies} from "nookies";

export const waitlists: Map<string, (accessToken: string) => void> = new Map();

export default async (req: NextApiRequest, res: NextApiResponse) => {
    // the actions need to be in the same file so Next doesn't compile them separately and split the waitlists map
    switch (req.query.action) {
        case "end": {
            const token = req.query.token as string;
            if (!token) return res.status(400).end("No token provided");

            const {accessToken} = parseCookies({req});
            if (!accessToken) return res.status(400).end("Cookies must be enabled");

            if (!waitlists.has(token)) return res.status(404).end("Token is invalid or expired");

            waitlists.get(token)(accessToken);
            res.status(200).end("You may close this page");

            break;
        }

        case "wait": {
            const token = req.query.token as string;
            if (!token) return res.status(400).json("{error:\"Missing token\"}");

            await new Promise((yay, nay) => {
                const timeout = setTimeout(() => {
                    waitlists.delete(token);
                    nay(new Error("Timeout"));
                }, 10 * 60 * 1000 /* 10min */);

                waitlists.set(token, accessToken => {
                    waitlists.delete(token);
                    res.status(200).json({token: accessToken});
                    clearTimeout(timeout);
                    yay();
                });
            });

            break;
        }

        default:
            res.status(404).end("Not found");
    }
};
