import {NextApiRequest, NextApiResponse} from "next";
import {destroyCookie} from "nookies";

export default async (req: NextApiRequest, res: NextApiResponse) => {
    destroyCookie({res}, "accessToken", {
        path: "/"
    });

    if (req.query.redirect) {
        res.setHeader("location", req.query.redirect);
        res.status(302);
    }

    res.end("You have been logged out.");
};
