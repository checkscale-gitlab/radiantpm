import {
    Box,
    Button, Divider,
    Flex,
    FormControl,
    FormHelperText,
    FormLabel, Grid,
    Heading,
    Input,
    Select, Spinner,
    Stack,
    Text, Tooltip
} from "@chakra-ui/core";
import {FeedFormat} from "../../lib/db/orm";
import {ChangeEvent, FormEvent, useCallback, useMemo, useState} from "react";
import {Diagnostic, Severity, Validator} from "../../lib/controlled-input";
import {useRouter} from "next/router";
import {GetServerSideProps} from "next";
import {inject} from "../../lib/dependency-injection";
import GitlabApi, {AccessLevel, GetGroupsItem} from "../../lib/GitlabApi";

const feedNameValidator: Validator<string> = val => {
    const diagnostics: Diagnostic[] = [];

    if (val.length === 0) diagnostics.push({
        message: "Feed name is required",
        type: "length",
        severity: Severity.Error
    });

    if (!/^[a-z0-9_-]+$/.test(val)) diagnostics.push({
        message: "Invalid feed name",
        type: "length",
        severity: Severity.Error
    });

    return diagnostics;
};

interface Props {
    groups: string[];
    loggedIn: boolean;
}

export default function(props: Props) {
    const feedFormatDescriptions = {
        [FeedFormat.npm]: "Node Package Manager",
        [FeedFormat.NuGet]: ".Net Package Manager"
    };

    const router = useRouter();

    const [feedType, setFeedType] = useState(FeedFormat.npm);
    const [feedName, setFeedName] = useState("");
    const [groupName, setGroupName] = useState("");
    const [description, setDescription] = useState("");

    const [creating, setCreating] = useState(false);
    const [error, setError] = useState({message: "", target: ""});

    const clearErrors = useCallback(() => setError({message: "", target: ""}), []);

    const handleSubmit = useCallback(async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        clearErrors();

        const formData = new FormData(e.currentTarget);

        setCreating(true);
        const res = await fetch("/api/feeds", {
            method: "post",
            body: formData
        });

        const json = await res.json();

        if (res.status === 400 || res.status === 409) {
            setCreating(false);
            setError({
                message: json.error,
                target: json.for
            });
        }

        if (res.status === 201) {
            router.push("/feeds/[feed]", `/feeds/${feedName}`);
        }
    }, [feedType, feedName, groupName]);

    const feedNameDiagnostics = useMemo(() => feedNameValidator(feedName), [feedName]);

    if (!props.loggedIn) {
        if (process.browser) router.replace("/login?redirect=/feeds/create");

        return (
            <Box>
                <Text>Redirecting to log-in page...</Text>
            </Box>
        );
    }

    return (
        <Box>
            <Heading>Create feed</Heading>

            <Stack>
                <Text>A feed is a list of packages that you can publish or install using a supported client.</Text>

                <form action="/api/feeds" method="POST" onSubmit={handleSubmit}>
                    <Grid templateColumns="3fr 1.5fr 0.7fr" gap={4} alignItems="end">
                        <FormControl gridRow="1" gridColumn="1"
                                     isRequired isInvalid={feedNameDiagnostics.length > 0 || error.target === "feedName"}>
                            <FormLabel htmlFor="feed-name">Feed name</FormLabel>
                            <Tooltip hasArrow isOpen={error.target === "feedName"} aria-label={error.message} label={error.message}>
                                <Input id="feed-name" name="feedName" value={feedName}
                                       onChange={(e: ChangeEvent<HTMLInputElement>) => { clearErrors(); setFeedName(e.target.value) }} />
                            </Tooltip>
                            <FormHelperText>Pick something short and unique, using only [a-z_-]</FormHelperText>
                        </FormControl>

                        <FormControl gridRow="1" gridColumn="2/4"
                                     isInvalid={error.target === "groupName"}>
                            <FormLabel htmlFor="group-name">GitLab group name</FormLabel>
                            <Box pos="relative">
                                <Select pos="absolute" top={0} left={0} right={0} bottom={0}
                                        onChange={(e: ChangeEvent<HTMLSelectElement>) => { clearErrors(); setGroupName(e.target.value) }}
                                        value={props.groups.includes(groupName) ? groupName : ""}
                                >
                                    {!props.groups.includes(groupName) ?
                                        <option value={groupName}>{groupName}</option> :
                                        <option value="" />
                                    }
                                    {props.groups.map(group => (
                                        <option key={group} value={group}>{group}</option>
                                    ))}
                                </Select>

                                <Tooltip hasArrow isOpen={error.target === "groupName"}
                                         aria-label={error.message} label={error.message}>
                                    <Input id="group-name" name="groupName" value={groupName}
                                           borderRight="none" width="calc(100% - 2.5rem)" roundedRight={0}
                                           onChange={(e: ChangeEvent<HTMLInputElement>) => { clearErrors(); setGroupName(e.target.value) }} />
                                </Tooltip>
                            </Box>
                            <FormHelperText>Limits packages to this group</FormHelperText>
                        </FormControl>

                        <FormControl>
                            <FormLabel htmlFor="description">Description</FormLabel>
                            <Input id="description" name="description" value={description}
                                   onChange={(e: ChangeEvent<HTMLInputElement>) => setDescription(e.target.value)} />
                        </FormControl>

                        <FormControl gridRow="2" gridColumn="2"
                                     isRequired isInvalid={error.target === "feedType"}>
                            <FormLabel htmlFor="feed-type">Feed type</FormLabel>
                            <Flex align="center">
                                <Box>
                                    <Select id="feed-type" name="feedType" value={feedType}
                                            onChange={e => { clearErrors(); setFeedType(parseInt(e.target.value)) }}>
                                        <option value={FeedFormat.npm}>npm</option>
                                        <option value={FeedFormat.NuGet}>NuGet</option>
                                    </Select>
                                </Box>
                                <Text flexGrow={1} fontSize="sm" color="gray.500" ml={2}>- {feedFormatDescriptions[feedType]}</Text>
                            </Flex>
                        </FormControl>

                        <Button gridRow="2" gridColumn="3"
                                type="submit" variant="outline" isDisabled={feedNameDiagnostics.length > 0}>
                            {creating ? <Spinner /> : "Create"}
                        </Button>
                    </Grid>
                </form>

                <Divider mt={6} ml={3} mr={3} />
            </Stack>
        </Box>
    );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
    const api: GitlabApi = inject(ctx, GitlabApi);
    const groups = await api?.getGroups({min_access_level: AccessLevel.Owner});

    return {
        props: {
            groups: groups?.map(group => group.full_name) || [],
            loggedIn: api != null
        }
    };
};
