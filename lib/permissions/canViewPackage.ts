import {ContextType, inject} from "../dependency-injection";
import GitlabApi, {AccessLevel} from "../GitlabApi";
import getPackageName, {GetPackageNameArg} from "../getPackageName";

export default async function canViewPackage(ctx: ContextType, pkg: GetPackageNameArg, existingApi?: GitlabApi, accessLevel = AccessLevel.Guest, nullIfNotFound = false) {
    if (!pkg.feed) throw new ReferenceError("Package must include feed information");

    const api: GitlabApi = existingApi || inject(ctx, GitlabApi);
    const projectPaths = Array.from(new Set(getPackageName(pkg)));

    let packg = null;

    // try some different package names
    for (const path of projectPaths) {
        console.debug("Trying project at", path);
        packg = await api?.getProject(path).catch(() => null);
        console.log("Got it?", !!packg);
        if (packg !== null) break;
    }

    if (packg == null) return nullIfNotFound ? null : false;

    return (packg.permissions.project_access || packg.permissions.group_access).access_level >= accessLevel || packg.visibility !== "private";
}
