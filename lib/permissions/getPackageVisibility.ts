import {Package} from "../db/orm";
import {ContextType, inject} from "../dependency-injection";
import GitlabApi from "../GitlabApi";
import getPackageName from "../getPackageName";

export default async function getPackageVisibility(ctx: ContextType, pkg: Package) {
    if (!pkg.feed) throw new ReferenceError("Package must include feed information");

    const api: GitlabApi = inject(ctx, GitlabApi);
    const projectPath = getPackageName(pkg);

    const packg = await api?.getProject(projectPath).catch(() => null);
    if (packg == null) return null;

    return packg.visibility;
}
