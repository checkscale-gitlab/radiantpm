import {IStringTransformer, transform} from "@alduino/humanizer/string";
import {FeedFormat} from "./db/orm";

const npmRegex = /^(?:@([^\/]+?))\/([^\/]+?)$/;

export interface GetPackageNameArg {
    name: string;
    feed: {format: FeedFormat};
}

const toUnderscoreSeparated: IStringTransformer = {
    transform(input: string): string {
        return input.replace(/([a-z])([A-Z])/g, "$1_$2");
    }
};

const toDashSeparated: IStringTransformer = {
    transform(input: string): string {
        return input.replace(/([a-z])([A-Z])/g, "$1-$2");
    }
};

export default function getPackageName(pkg: GetPackageNameArg): string[] {
    switch (pkg.feed.format) {
        case FeedFormat.npm: {
            if (!npmRegex.test(pkg.name)) return [pkg.name];
            const [, org, name] = pkg.name.match(npmRegex);
            return [org + "/" + name.split("_").filter(v => v.trim().length > 0).join("/")];
        }
        case FeedFormat.NuGet: {
            return [
                pkg.name.replace(/\./g, "/"),
                pkg.name.replace(/\./g, "/").replace(/[^a-z\/]/gi, ""),
                pkg.name.substring(0, pkg.name.indexOf(".")) + "/" + pkg.name.substring(pkg.name.indexOf(".") + 1).replace(/\./g, "-"),
                pkg.name.substring(0, pkg.name.indexOf(".")) + "/" + pkg.name.substring(pkg.name.indexOf(".") + 1).replace(/\./g, "_"),
                pkg.name.substring(0, pkg.name.indexOf(".")) + "/" + pkg.name.substring(pkg.name.indexOf(".") + 1).replace(/\./g, "/"),
                transform(pkg.name.substring(0, pkg.name.indexOf(".")), toDashSeparated) + "/" + pkg.name.substring(pkg.name.indexOf(".") + 1).replace(/\./g, "_"),
                transform(pkg.name.substring(0, pkg.name.indexOf(".")), toDashSeparated) + "/" + pkg.name.substring(pkg.name.indexOf(".") + 1).replace(/\./g, "_"),
                transform(pkg.name.substring(0, pkg.name.indexOf(".")), toDashSeparated) + "/" + pkg.name.substring(pkg.name.indexOf(".") + 1).replace(/\./g, "/"),
            ];
        }
        default: return [pkg.name];
    }
}
