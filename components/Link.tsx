import {Box, Icon, Link as ChakraLink, LinkProps as ChakraLinkProps, Spinner} from "@chakra-ui/core";
import {useRouter} from "next/router";
import {MouseEvent, useCallback} from "react";
import {useAsync} from "@react-hook/async";

interface LinkProps {
    linkMap?: string;
    replace?: boolean;
    query?: {[key: string]: string};
}

export default function Link(props: ChakraLinkProps & LinkProps) {
    const router = useRouter();
    const {href, linkMap, replace, onClick, query} = props;

    const urlWithQuery = new URL(linkMap || href, "http://fake.url");

    if (query) {
        for (const key of Object.keys(query)) {
            urlWithQuery.searchParams.append(key, query[key]);
        }
    }

    const urlWithQueryStr = urlWithQuery.toString().substring("http://fake.url".length);

    const [{status}, push] = useAsync((href: string, as?: string, replace?: boolean) => {
        return router.push(href, as, {
            replace,
            query
        });
    });

    const handleClick = useCallback((ev: MouseEvent<HTMLAnchorElement>) => {
        if (onClick) onClick(ev);
        if (!urlWithQueryStr.startsWith("/") || urlWithQueryStr.startsWith("/api/")) return;

        ev.preventDefault();
        push(href, urlWithQueryStr, replace);
    }, [router, href, linkMap, onClick]);

    const handlePrefetch = useCallback(() => {
        if (!urlWithQueryStr.startsWith("/") || urlWithQueryStr.startsWith("/api/")) return;

        router.prefetch(href, urlWithQueryStr, {
            priority: true
        });
    }, [href, linkMap]);

    const opacity = status === "loading" || status === "error" ? 0 : 1;

    return (
        <Box pos="relative" as="span">
            <ChakraLink {...props} href={urlWithQueryStr} opacity={opacity}
                        onClick={handleClick} onMouseEnter={handlePrefetch} />
            {status === "loading" && (<Spinner as="span" pos="absolute" left={0} />)}
            {status === "error" && (<Icon name="warning" pos="absolute" left={0} />)}
        </Box>
    )
;
}
