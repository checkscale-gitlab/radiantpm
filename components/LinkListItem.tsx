import {createContext, FC} from "react";
import {Box, Divider, Flex, Heading, IconButton, Stack, Text} from "@chakra-ui/core";
import NextLink from "next/link";
import Link from "./Link";

interface LinkListContext {
    link: string;
    linkAs?: string;
}

const LinkListContext = createContext<LinkListContext>({link: "#"});
LinkListContext.displayName = "LinkListItem";

export interface LinkListItemProps {
    title: string;
    link: string;
    linkAs?: string;
    detail: string;
    description: string;
}

export const ExtraInfo: FC = ({children}) => (
    <>
        <Divider orientation="vertical" height={4} />
        {children}
    </>
);

export const Buttons: FC = ({children}) => (
    <>
        <Box width="auto" flexGrow={1} />
        {children}
        <LinkListContext.Consumer>
            {v => (
                <NextLink href={v.link} as={v.linkAs}>
                    <a>
                        <IconButton aria-label="Open" icon="chevron-right" size="sm" />
                    </a>
                </NextLink>
            )}
        </LinkListContext.Consumer>
    </>
);

export const LinkListItem: FC<LinkListItemProps> = props => (
    <LinkListContext.Provider value={{link: props.link, linkAs: props.linkAs}}>
        <Stack borderWidth="1px" rounded="md" padding={2} marginTop={4}>
            <Flex align="center">
                <Heading as="h3" fontSize="md">
                    <Link href={props.link} linkMap={props.linkAs}>{props.title}</Link>
                </Heading>
                <Text fontSize="sm" ml={2}>{props.detail}</Text>
                {props.children}
            </Flex>
            {props.description && <Text mt={-2}>{props.description}</Text>}
        </Stack>
    </LinkListContext.Provider>
);
