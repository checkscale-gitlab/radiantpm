import React, {FC} from "react";
import {css, cx} from "emotion";
import {ControlledInputProps, ShowsDiagnostics, getDiagnostics} from "../lib/controlled-input";
import {PuffLoader} from "react-spinners";

export interface TextInputProps {
    valueHidden?: boolean;
    placeholder?: string;
}

const inputStyle = css`
    background: transparent;
    border: none;
    
    padding: .5em 1em;
`;

const errorStyle = css`
    font-size: .7em;
    display: flex;
    flex-direction: row;
    border-top: 2px solid var(--border-colour);
`;

const errorCountStyle = css`
    background: var(--border-colour);
    color: white;
    padding: .5em 1em;
`;

const errorMessageStyle = css`
    padding: .5em 1em;
`;

const inputContainerStyle = css`
    --border-colour: #223;
    
    background: #fafaff;
    border: 2px solid var(--border-colour);
    
    display: inline-block;
`;

const inputContainerErrorStyle = css`
    --border-colour: #d05050;
`;

const inputContainerDisabledStyle = css`
    --border-colour: #7778;
`;

const inputContainerLoadingStyle = css`
    display: inline-flex;
    align-items: center;
    flex-direction: row;
`;

const loadingIconStyle = css`
    width: 0;
    padding-left: .5em;
    padding-right: 2em;
    height: 1.5em;
`;

export const TextInput: FC<ControlledInputProps<string> & ShowsDiagnostics<string> & TextInputProps> = props => {
    const diagnostics = getDiagnostics(props);

    const disabled = props.disabled || props.loading;

    return (
        <div className={cx(inputContainerStyle, {
            [inputContainerErrorStyle]: diagnostics.length > 0,
            [inputContainerDisabledStyle]: disabled,
            [inputContainerLoadingStyle]: props.loading
        })}>
            <input
                type={props.valueHidden ? "password" : "text"}
                placeholder={props.placeholder}

                name={props.name}
                value={props.value}
                disabled={disabled}
                onChange={ev => props.onChange(ev.target.value)}

                className={inputStyle}
            />
            {!disabled && diagnostics.length > 0 && (
                <div className={errorStyle}>
                    <div className={errorCountStyle}>
                        {diagnostics.length} error{diagnostics.length > 1 ? "s" : ""}:
                    </div>
                    <div className={errorMessageStyle}>
                        {diagnostics[0].message}
                    </div>
                </div>
            )}
            {props.loading && (
                <div className={loadingIconStyle}>
                    <PuffLoader size="1em" />
                </div>
            )}
        </div>
    );
};
